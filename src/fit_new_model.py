"""
code to generate preprocessing module for the titanic dataset
DO NOT TOUCH
"""
import sys

from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
import pandas as pd
import joblib

DROP_COLS = ["Name", "Ticket", "Cabin", "Embarked", "Sex"]
FILL_COLS = ["Fare", "Age", "Parch", "Pclass", "SibSp"]


def get_pipeline():
    ct = ColumnTransformer(
        [
            ("drop", "drop", DROP_COLS),
            # ("sex", FunctionTransformer(lambda X: (X == "female").to_frame()), "Sex"),
            ("fillna_with_mean", SimpleImputer(strategy="mean"), FILL_COLS),
        ]
    )

    return make_pipeline(ct, LogisticRegression())


if __name__ == "__main__":
    if not len(sys.argv) == 3:
        raise ValueError(
            "you must pass the training as first arg and the model path as second"
        )

    pipe = get_pipeline()
    df = pd.read_csv(sys.argv[1], index_col="PassengerId")
    X, y = df.drop("Survived", axis=1), df["Survived"]
    pipe.fit(X, y)
    joblib.dump(pipe, sys.argv[2])
